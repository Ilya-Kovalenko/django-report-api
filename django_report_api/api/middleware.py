from api.finam_persistent_storage import report_processing


class ReportsMiddleware:
    def __init__(self, get_response):
        self._get_response = get_response

    def __call__(self, request):
        response = self._get_response(request)

        if request.path == "/api/reports/":
            if response.status_code == 201 and hasattr(response, 'data'):
                s = response.data['file']
                file_path = s[s.rindex('/') + 1:]
                report_processing.process("reports/" + file_path)
        print("We are after our middleware")

        return response
