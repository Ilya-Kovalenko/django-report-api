from rest_framework.serializers import ModelSerializer
from .models import ReportFile


class ReportFileSerializer(ModelSerializer):
    class Meta:
        model = ReportFile
        fields = ('name', 'file')
