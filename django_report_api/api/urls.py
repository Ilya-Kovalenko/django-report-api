from django.urls import include, path

from rest_framework.routers import SimpleRouter
from .views import ReportFileViewSet


router = SimpleRouter()
router.register(r'reports', ReportFileViewSet, basename='reports')

urlpatterns = [
    path('', include(router.urls)),
]
