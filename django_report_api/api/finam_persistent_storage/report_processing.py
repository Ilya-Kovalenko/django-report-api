from .migrations import migrate
from .storage import Storage
import sqlite3


def process(file_path: str = "report.xml"):
    migrate()

    storage = Storage(file_path)

    storage.save_account_info()
    storage.save_account_state()
    storage.save_account_operations()
    storage.save_securities_state()
    storage.save_cash_transactions()
    storage.save_non_trading_securities_operations()
    storage.save_other_transactions()
    storage.save_cash_state()
    storage.save_cash_flow_types()
    storage.save_trading_fees()
    storage.save_transfer_positions_costs()
    storage.save_repo_differences()
    storage.save_show_section()
    storage.save_financial_instruments_evaluation()
    storage.save_pending_transactions_evaluation()

    sqlite_connection = sqlite3.connect('database.db')
    cursor = sqlite_connection.cursor()
    sqlite_select_table_query = """SELECT * FROM account_state"""
    result = cursor.execute(sqlite_select_table_query).fetchall()
    sqlite_connection.commit()
    cursor.close()
    sqlite_connection.close()
    print(result)


if __name__ == "__main__":
    process()
