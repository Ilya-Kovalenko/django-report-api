import sqlite3


def migrate():
    try:
        sqlite_connection = sqlite3.connect('database.db')
        cursor = sqlite_connection.cursor()

        sqlite_create_table_query = '''CREATE TABLE IF NOT EXISTS account_info (
                                        id INTEGER PRIMARY KEY AUTOINCREMENT,
                                        account_name TEXT NOT NULL,
                                        account_id TEXT NOT NULL UNIQUE,
                                        client_info TEXT NOT NULL,
                                        contract_info TEXT NOT NULL,
                                        requisites_title TEXT NOT NULL,
                                        requisites_text TEXT NOT NULL,
                                        requisites_firm TEXT NOT NULL,
                                        requisites_date_begin TEXT NOT NULL,
                                        requisites_date_end TEXT NOT NULL,
                                        requisites_date_create TEXT NOT NULL,
                                        requisites_amount_days INTEGER NOT NULL);'''
        cursor.execute(sqlite_create_table_query)
        sqlite_connection.commit()

        sqlite_create_table_query = '''CREATE TABLE IF NOT EXISTS account_state (
                                        id INTEGER PRIMARY KEY AUTOINCREMENT,
                                        total_name TEXT NOT NULL,
                                        total_B REAL NOT NULL,
                                        total_E REAL NOT NULL,
                                        total_delta REAL NOT NULL,
                                        ruble_position_name TEXT NOT NULL,
                                        ruble_position_B REAL NOT NULL,
                                        ruble_position_E REAL NOT NULL,
                                        ruble_position_delta REAL NOT NULL,
                                        currency_position_name TEXT NOT NULL,
                                        currency_position_B REAL NOT NULL,
                                        currency_position_E REAL NOT NULL,
                                        currency_position_delta REAL NOT NULL,
                                        pfi_evaluation_name TEXT NOT NULL,
                                        pfi_evaluation_B REAL NOT NULL,
                                        pfi_evaluation_E REAL NOT NULL,
                                        pfi_evaluation_delta REAL NOT NULL,
                                        securities_name TEXT NOT NULL,
                                        securities_B REAL NOT NULL,
                                        securities_E REAL NOT NULL,
                                        securities_delta REAL NOT NULL,
                                        pending_liabilities_name TEXT NOT NULL,
                                        pending_liabilities_B REAL NOT NULL,
                                        pending_liabilities_E REAL NOT NULL,
                                        pending_liabilities_delta REAL NOT NULL,
                                        planned_positions_name TEXT NOT NULL,
                                        planned_positions_B REAL NOT NULL,
                                        planned_positions_E REAL NOT NULL,
                                        planned_positions_delta REAL NOT NULL);'''
        cursor.execute(sqlite_create_table_query)
        sqlite_connection.commit()

        sqlite_create_table_query = '''CREATE TABLE IF NOT EXISTS account_operations (
                                        account_operations_id INTEGER PRIMARY KEY,
                                        summary_name TEXT NOT NULL,
                                        summary_fee REAL NOT NULL,
                                        summary_curF TEXT NOT NULL);'''
        cursor.execute(sqlite_create_table_query)
        sqlite_connection.commit()

        sqlite_create_table_query = '''CREATE TABLE IF NOT EXISTS operations (
                                        operation_id INTEGER PRIMARY KEY AUTOINCREMENT,
                                        account_operations_id INTEGER NOT NULL,
                                        D TEXT NOT NULL,
                                        T TEXT NOT NULL,
                                        IS_Data TEXT NOT NULL,
                                        ISIN TEXT NOT NULL,
                                        Op TEXT NOT NULL,
                                        Qty REAL NOT NULL,
                                        Pr REAL NOT NULL,
                                        Cur TEXT NOT NULL,
                                        SPr REAL NOT NULL,
                                        SPrA REAL NOT NULL,
                                        CurP TEXT NOT NULL,
                                        Rt REAL NOT NULL,
                                        SPay REAL NOT NULL,
                                        DebtP REAL NOT NULL,
                                        DebtD REAL NOT NULL,
                                        DPayP TEXT NOT NULL,
                                        DDelP TEXT NOT NULL,
                                        DPayF TEXT NOT NULL,
                                        DDelF TEXT NOT NULL,
                                        M TEXT NOT NULL,
                                        P TEXT NOT NULL,
                                        RqNo INTEGER NOT NULL,
                                        TrdN INTEGER NOT NULL,
                                        I TEXT NOT NULL,
                                        C TEXT NOT NULL,
                                        FOREIGN KEY(account_operations_id) REFERENCES account_operations(account_operations_id));'''
        cursor.execute(sqlite_create_table_query)
        sqlite_connection.commit()

        sqlite_create_table_query = '''CREATE TABLE IF NOT EXISTS commissions (
                                        commission_id INTEGER PRIMARY KEY AUTOINCREMENT,
                                        account_operations_id INTEGER NOT NULL,
                                        D TEXT NOT NULL,
                                        T TEXT NOT NULL,
                                        Op TEXT NOT NULL,
                                        Fee REAL NOT NULL,
                                        CurF TEXT NOT NULL,
                                        DPayF TEXT NOT NULL,
                                        C TEXT NOT NULL,
                                        FOREIGN KEY(account_operations_id) REFERENCES account_operations(account_operations_id));'''
        cursor.execute(sqlite_create_table_query)
        sqlite_connection.commit()

        sqlite_create_table_query = '''CREATE TABLE IF NOT EXISTS securities_state (
                                        securities_state_id INTEGER PRIMARY KEY,
                                        summary_name TEXT NOT NULL,
                                        summary_CostB REAL NOT NULL,
                                        summary_CostE REAL NOT NULL);'''
        cursor.execute(sqlite_create_table_query)
        sqlite_connection.commit()

        sqlite_create_table_query = '''CREATE TABLE IF NOT EXISTS securities (
                                        security_id INTEGER PRIMARY KEY AUTOINCREMENT,
                                        securities_state_id INTEGER NOT NULL,
                                        I TEXT NOT NULL,
                                        IS_Data TEXT NOT NULL,
                                        ISIN TEXT NOT NULL,
                                        In_Data REAL NOT NULL,
                                        BlkB REAL NOT NULL,
                                        CostB REAL NOT NULL,
                                        Inc REAL NOT NULL,
                                        Dec REAL NOT NULL,
                                        Out REAL NOT NULL,
                                        BlkE REAL NOT NULL,
                                        CostE REAL NOT NULL,
                                        P TEXT NOT NULL,
                                        T1 REAL NOT NULL,
                                        T2 REAL NOT NULL,
                                        T3 REAL NOT NULL,
                                        TN REAL NOT NULL,
                                        FOREIGN KEY(securities_state_id) REFERENCES securities_state(securities_state_id));'''
        cursor.execute(sqlite_create_table_query)
        sqlite_connection.commit()

        sqlite_create_table_query = '''CREATE TABLE IF NOT EXISTS cash_transactions (
                                        id INTEGER PRIMARY KEY AUTOINCREMENT,
                                        D TEXT NOT NULL,
                                        Op TEXT NOT NULL,
                                        Cur TEXT,
                                        Inc REAL,
                                        Dec REAL,
                                        Rub REAL NOT NULL,
                                        C TEXT);'''
        cursor.execute(sqlite_create_table_query)
        sqlite_connection.commit()

        sqlite_create_table_query = '''CREATE TABLE IF NOT EXISTS non_trading_securities_operations (
                                        id INTEGER PRIMARY KEY AUTOINCREMENT,
                                        D TEXT NOT NULL,
                                        I TEXT NOT NULL,
                                        Op TEXT NOT NULL,
                                        ISIN TEXT NOT NULL,
                                        Inc REAL,
                                        Dec REAL);'''
        cursor.execute(sqlite_create_table_query)
        sqlite_connection.commit()

        sqlite_create_table_query = '''CREATE TABLE IF NOT EXISTS other_transactions_summary (
                                        other_transactions_summary_id INTEGER PRIMARY KEY,
                                        summary_Name TEXT NOT NULL,
                                        summary_Fee REAL NOT NULL,
                                        summary_CurF TEXT NOT NULL);'''
        cursor.execute(sqlite_create_table_query)
        sqlite_connection.commit()

        sqlite_create_table_query = '''CREATE TABLE IF NOT EXISTS other_transactions (
                                        other_transaction_id INTEGER PRIMARY KEY AUTOINCREMENT,
                                        other_transactions_summary_id INTEGER NOT NULL,
                                        D TEXT NOT NULL,
                                        T TEXT NOT NULL,
                                        IS_Data TEXT NOT NULL,
                                        Type TEXT NOT NULL,
                                        Qty REAL NOT NULL,
                                        Pr REAL NOT NULL,
                                        Cur TEXT NOT NULL,
                                        ACY REAL NOT NULL,
                                        SPr REAL NOT NULL,
                                        SPrA REAL NOT NULL,
                                        CurP TEXT NOT NULL,
                                        Rt REAL NOT NULL,
                                        SPay REAL NOT NULL,
                                        Fee REAL NOT NULL,
                                        CurF TEXT NOT NULL,
                                        DebtP REAL NOT NULL,
                                        DebtD REAL NOT NULL,
                                        DPayP TEXT NOT NULL,
                                        DDelP TEXT NOT NULL,
                                        DPayF TEXT NOT NULL,
                                        DDelF TEXT NOT NULL,
                                        P TEXT NOT NULL,
                                        TrdN INTEGER NOT NULL,
                                        FOREIGN KEY(other_transactions_summary_id) REFERENCES other_transactions_summary(other_transactions_summary_id));'''
        cursor.execute(sqlite_create_table_query)
        sqlite_connection.commit()

        sqlite_create_table_query = '''CREATE TABLE IF NOT EXISTS cash_state (
                                        cash_state_id INTEGER PRIMARY KEY,
                                        summary_Name TEXT NOT NULL,
                                        summary_TagBSum REAL NOT NULL,
                                        summary_TagESum REAL NOT NULL);'''
        cursor.execute(sqlite_create_table_query)
        sqlite_connection.commit()

        sqlite_create_table_query = '''CREATE TABLE IF NOT EXISTS currencies (
                                        currency_id INTEGER PRIMARY KEY AUTOINCREMENT,
                                        cash_state_id INTEGER NOT NULL,
                                        name TEXT NOT NULL,
                                        t1 REAL NOT NULL,
                                        t2 REAL NOT NULL,
                                        t3 REAL NOT NULL,
                                        tn REAL NOT NULL,
                                        tag_b_SumC REAL NOT NULL,
                                        tag_b_Free REAL NOT NULL,
                                        tag_b_Blk REAL NOT NULL,
                                        tag_b_Mrg REAL NOT NULL,
                                        tag_b_Debt REAL NOT NULL,
                                        tag_b_Tax REAL NOT NULL,
                                        tag_b_Sum REAL NOT NULL,
                                        tag_e_SumC REAL NOT NULL,
                                        tag_e_Free REAL NOT NULL,
                                        tag_e_Blk REAL NOT NULL,
                                        tag_e_Mrg REAL NOT NULL,
                                        tag_e_Debt REAL NOT NULL,
                                        tag_e_Tax REAL NOT NULL,
                                        tag_e_Sum REAL NOT NULL,
                                        FOREIGN KEY(cash_state_id) REFERENCES cash_state(cash_state_id));'''
        cursor.execute(sqlite_create_table_query)
        sqlite_connection.commit()

        sqlite_create_table_query = '''CREATE TABLE IF NOT EXISTS cash_flow_types (
                                        id INTEGER PRIMARY KEY AUTOINCREMENT,
                                        Name TEXT NOT NULL,
                                        MI REAL NOT NULL,
                                        MO REAL NOT NULL,
                                        SNE REAL NOT NULL,
                                        SE REAL NOT NULL,
                                        SV REAL NOT NULL,
                                        BNE REAL NOT NULL,
                                        BE REAL NOT NULL,
                                        BV REAL NOT NULL,
                                        VM REAL NOT NULL,
                                        GO REAL NOT NULL,
                                        C REAL NOT NULL,
                                        D REAL NOT NULL,
                                        BC REAL NOT NULL,
                                        AC REAL NOT NULL,
                                        Tax REAL NOT NULL,
                                        TaxKeep REAL NOT NULL,
                                        Oth REAL NOT NULL);'''
        cursor.execute(sqlite_create_table_query)
        sqlite_connection.commit()

        sqlite_create_table_query = '''CREATE TABLE IF NOT EXISTS trading_fees (
                                        id INTEGER PRIMARY KEY AUTOINCREMENT,
                                        Op TEXT NOT NULL,
                                        CurF TEXT NOT NULL,
                                        Fee REAL NOT NULL,
                                        C TEXT NOT NULL);'''
        cursor.execute(sqlite_create_table_query)
        sqlite_connection.commit()

        sqlite_create_table_query = '''CREATE TABLE IF NOT EXISTS transfer_positions_costs (
                                        id INTEGER PRIMARY KEY AUTOINCREMENT,
                                        Op TEXT NOT NULL,
                                        Cur TEXT NOT NULL,
                                        Fee REAL NOT NULL,
                                        C TEXT NOT NULL);'''
        cursor.execute(sqlite_create_table_query)
        sqlite_connection.commit()

        sqlite_create_table_query = '''CREATE TABLE IF NOT EXISTS repo_differences (
                                        repo_differences_id INTEGER PRIMARY KEY);'''
        cursor.execute(sqlite_create_table_query)
        sqlite_connection.commit()

        sqlite_create_table_query = '''CREATE TABLE IF NOT EXISTS repo_differences_r (
                                        repo_differences_r_id INTEGER PRIMARY KEY AUTOINCREMENT,
                                        repo_differences_id INTEGER NOT NULL,
                                        G TEXT NOT NULL,
                                        Type TEXT NOT NULL,
                                        D TEXT NOT NULL,
                                        IS_Data TEXT NOT NULL,
                                        V TEXT NOT NULL,
                                        Qty REAL NOT NULL,
                                        PrS REAL NOT NULL,
                                        SumS REAL NOT NULL,
                                        PrF REAL NOT NULL,
                                        SumF REAL NOT NULL,
                                        SumDif REAL NOT NULL,
                                        FOREIGN KEY(repo_differences_id) REFERENCES repo_differences(repo_differences_id));'''
        cursor.execute(sqlite_create_table_query)
        sqlite_connection.commit()

        sqlite_create_table_query = '''CREATE TABLE IF NOT EXISTS repo_differences_s (
                                        repo_differences_s_id INTEGER PRIMARY KEY AUTOINCREMENT,
                                        repo_differences_id INTEGER NOT NULL,
                                        G TEXT NOT NULL,
                                        D TEXT NOT NULL,
                                        IS_Data TEXT NOT NULL,
                                        V TEXT NOT NULL,
                                        SumS REAL NOT NULL,
                                        SumF REAL NOT NULL,
                                        SumDif REAL NOT NULL,
                                        FOREIGN KEY(repo_differences_id) REFERENCES repo_differences(repo_differences_id));'''
        cursor.execute(sqlite_create_table_query)
        sqlite_connection.commit()

        sqlite_create_table_query = '''CREATE TABLE IF NOT EXISTS show_section (
                                        id INTEGER PRIMARY KEY AUTOINCREMENT,
                                        sh4 INTEGER NOT NULL,
                                        sh5 INTEGER NOT NULL,
                                        sh6 INTEGER NOT NULL,
                                        sh7 INTEGER NOT NULL,
                                        sh8 INTEGER NOT NULL,
                                        sh9 INTEGER NOT NULL,
                                        sh10 INTEGER NOT NULL,
                                        sh11 INTEGER NOT NULL,
                                        sh12 INTEGER NOT NULL,
                                        sh21 INTEGER NOT NULL,
                                        sh22 INTEGER NOT NULL,
                                        sh23 INTEGER NOT NULL,
                                        sh_ref INTEGER NOT NULL,
                                        sh_repo_mma INTEGER NOT NULL,
                                        sh_swap INTEGER NOT NULL);'''
        cursor.execute(sqlite_create_table_query)
        sqlite_connection.commit()

        sqlite_create_table_query = '''CREATE TABLE IF NOT EXISTS financial_instruments_evaluation (
                                        id INTEGER PRIMARY KEY AUTOINCREMENT,
                                        name TEXT NOT NULL,
                                        b_go REAL NOT NULL,
                                        e_go REAL NOT NULL,
                                        bgo REAL NOT NULL,
                                        ego REAL NOT NULL);'''
        cursor.execute(sqlite_create_table_query)
        sqlite_connection.commit()

        sqlite_create_table_query = '''CREATE TABLE IF NOT EXISTS pending_transactions_evaluation (
                                        id INTEGER PRIMARY KEY AUTOINCREMENT,
                                        name TEXT NOT NULL,
                                        evalb_cb REAL NOT NULL,
                                        evale_cb REAL NOT NULL,
                                        evalb_pl REAL NOT NULL,
                                        evale_pl REAL NOT NULL);'''
        cursor.execute(sqlite_create_table_query)
        sqlite_connection.commit()

        cursor.close()
        sqlite_connection.close()

    except sqlite3.Warning as war:
        print("Возникло исключение:", war)
    except sqlite3.Error as err:
        print("Возникла ошибка:", err)
