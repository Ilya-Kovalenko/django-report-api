import sqlite3
from .parser import XmlParser, RepoDifferenceR, RepoDifferenceS
from .converter import convert_date, convert_time


class Storage:
    def __init__(self, path: str):
        self.path = path
        self.xml_parser = XmlParser(path)

    def save_account_info(self):
        doc_header = self.xml_parser.get_account_info()

        date_begin = convert_date(doc_header.requisites.date_begin)
        date_end = convert_date(doc_header.requisites.date_end)
        date_create = convert_date(doc_header.requisites.date_create)

        try:
            sqlite_connection = sqlite3.connect('database.db')
            cursor = sqlite_connection.cursor()
            sqlite_insert_query = """INSERT INTO account_info
                                                      (account_name, account_id, client_info, contract_info, requisites_title, 
                                                      requisites_text, requisites_firm, requisites_date_begin, requisites_date_end, 
                                                      requisites_date_create, requisites_amount_days)
                                                      VALUES
                                                      ('{:s}', '{:s}', '{:s}', '{:s}', '{:s}', '{:s}', '{:s}', '{:s}', '{:s}', '{:s}',
                                                       {:d});""".format(doc_header.account_info.name, doc_header.account_info.id,
                                                                        doc_header.client_info, doc_header.contract_info,
                                                                        doc_header.requisites.title, doc_header.requisites.text,
                                                                        doc_header.requisites.firm, date_begin, date_end,
                                                                        date_create, doc_header.requisites.amount_days)
            cursor.execute(sqlite_insert_query)

            sqlite_connection.commit()
            cursor.close()
            sqlite_connection.close()
        except sqlite3.Warning as war:
            print("Возникло исключение:", war)
        except sqlite3.Error as err:
            print("Возникла ошибка:", err)

    def save_account_state(self):
        account_state = self.xml_parser.get_account_state()

        try:
            sqlite_connection = sqlite3.connect('database.db')
            cursor = sqlite_connection.cursor()

            sqlite_insert_query = """INSERT INTO account_state
                                              (total_name, total_B, total_E, total_delta, ruble_position_name,  ruble_position_B, 
                                              ruble_position_E, ruble_position_delta, currency_position_name,  currency_position_B, 
                                              currency_position_E, currency_position_delta, pfi_evaluation_name,  pfi_evaluation_B, 
                                              pfi_evaluation_E , pfi_evaluation_delta, securities_name, securities_B , securities_E, 
                                              securities_delta, pending_liabilities_name, pending_liabilities_B, pending_liabilities_E, 
                                              pending_liabilities_delta, planned_positions_name, planned_positions_B, 
                                              planned_positions_E, planned_positions_delta)
                                              VALUES
                                              ('{:s}', {:f}, {:f}, {:f}, '{:s}', {:f}, {:f}, {:f}, '{:s}', {:f}, {:f}, {:f}, 
                                              '{:s}', {:f}, {:f}, {:f}, '{:s}', {:f}, {:f}, {:f}, '{:s}', {:f}, {:f}, {:f}, 
                                              '{:s}', {:f}, {:f}, 
                                              {:f});""".format(account_state.total.Name, float(account_state.total.B), float(account_state.total.E),
                                                               float(account_state.total.Delta), account_state.ruble_position.Name,
                                                               float(account_state.ruble_position.B), float(account_state.ruble_position.E),
                                                               float(account_state.ruble_position.Delta), account_state.currency_position.Name,
                                                               float(account_state.currency_position.B), float(account_state.currency_position.E),
                                                               float(account_state.currency_position.Delta), account_state.pfi_evaluation.Name,
                                                               float(account_state.pfi_evaluation.B), float(account_state.pfi_evaluation.E),
                                                               float(account_state.pfi_evaluation.Delta), account_state.securities.Name,
                                                               float(account_state.securities.B), float(account_state.securities.E),
                                                               float(account_state.securities.Delta), account_state.pending_liabilities.Name,
                                                               float(account_state.pending_liabilities.B), float(account_state.pending_liabilities.E),
                                                               float(account_state.pending_liabilities.Delta), account_state.planned_positions.Name,
                                                               float(account_state.planned_positions.B), float(account_state.planned_positions.E),
                                                               float(account_state.planned_positions.Delta))
            cursor.execute(sqlite_insert_query)
            sqlite_connection.commit()
            cursor.close()
            sqlite_connection.close()
        except sqlite3.Warning as war:
            print("Возникло исключение:", war)
        except sqlite3.Error as err:
            print("Возникла ошибка:", err)

    def save_account_operations(self):
        account_operations = self.xml_parser.get_operations()

        try:
            sqlite_connection = sqlite3.connect('database.db')
            cursor = sqlite_connection.cursor()
            sqlite_select_query = """SELECT MAX(account_operations_id) FROM account_operations;"""
            cursor.execute(sqlite_select_query)

            last_id = cursor.fetchall()[0][0]

            if last_id is None:
                current_id = 0
            else:
                current_id = last_id + 1

            sqlite_insert_query = """INSERT INTO account_operations
                                            (account_operations_id, summary_name, summary_fee, summary_curF)
                                            VALUES
                                            ({:d}, '{:s}', {:f}, '{:s}');""".format(current_id, account_operations.summary.Name,
                                                                                 account_operations.summary.Fee, account_operations.summary.CurF)
            cursor.execute(sqlite_insert_query)

            for operation in account_operations.operations:
                date_D = convert_date(operation.D)
                time_T = convert_time(operation.T)
                date_DPayP = convert_date(operation.DPayP)
                date_DDelP = convert_date(operation.DDelP)
                date_DPayF = convert_date(operation.DPayF)
                date_DDelF = convert_date(operation.DDelF)

                sqlite_insert_query = """INSERT INTO operations
                                                        (account_operations_id, D, T, IS_Data, ISIN, Op, Qty, Pr, Cur, SPr, SPrA, 
                                                        CurP, Rt, SPay, DebtP, DebtD, DPayP, DDelP, DPayF, DDelF, M, P, RqNo, 
                                                        TrdN, I, C)
                                                        VALUES
                                                        ({:d}, '{:s}', '{:s}', '{:s}', '{:s}', '{:s}', {:f}, {:f}, '{:s}', {:f}, {:f}, 
                                                        '{:s}', {:f}, {:f}, {:f}, {:f}, '{:s}', '{:s}', '{:s}', '{:s}', '{:s}', '{:s}', 
                                                        {:d}, {:d}, 
                                                        '{:s}', '{:s}');""".format(current_id, date_D, time_T, operation.IS,
                                                                               operation.ISIN, operation.Op, operation.Qty,
                                                                               operation.Pr, operation.Cur, operation.SPr,
                                                                               operation.SPrA, operation.CurP, operation.Rt,
                                                                               operation.SPay, operation.DebtP, operation.DebtD,
                                                                               date_DPayP, date_DDelP, date_DPayF, date_DDelF,
                                                                               operation.M, operation.P, operation.RqNo,
                                                                               operation.TrdN, operation.I, operation.C)
                cursor.execute(sqlite_insert_query)

            for commission in account_operations.commissions:
                date_D = convert_date(commission.D)
                time_T = convert_time(commission.T)
                date_DPayF = convert_date(commission.DPayF)

                sqlite_insert_query = """INSERT INTO commissions
                                                        (account_operations_id, D, T, Op, Fee, CurF, DPayF, C)
                                                        VALUES
                                                        ({:d}, '{:s}', '{:s}', '{:s}', {:f}, '{:s}', '{:s}', '{:s}'
                                                        );""".format(current_id, date_D, time_T, commission.Op,
                                                                     commission.Fee, commission.CurF, date_DPayF, commission.C)
                cursor.execute(sqlite_insert_query)

            sqlite_connection.commit()
            cursor.close()
            sqlite_connection.close()
        except sqlite3.Warning as war:
            print("Возникло исключение:", war)
        except sqlite3.Error as err:
            print("Возникла ошибка:", err)

    def save_securities_state(self):
        securities_state = self.xml_parser.get_securities_state()

        try:
            sqlite_connection = sqlite3.connect('database.db')
            cursor = sqlite_connection.cursor()
            sqlite_select_query = """SELECT MAX(securities_state_id) FROM securities_state;"""
            cursor.execute(sqlite_select_query)

            last_id = cursor.fetchall()[0][0]

            if last_id is None:
                current_id = 0
            else:
                current_id = last_id + 1

            sqlite_insert_query = """INSERT INTO securities_state
                                                    (securities_state_id, summary_name, summary_CostB, summary_CostE)
                                                    VALUES
                                                    ({:d}, '{:s}', {:f}, {:f}
                                                    );""".format(current_id, securities_state.summary.Name,
                                                                 securities_state.summary.CostB, securities_state.summary.CostE)
            cursor.execute(sqlite_insert_query)

            for security in securities_state.securities:
                sqlite_insert_query = """INSERT INTO securities
                                                        (securities_state_id, I, IS_Data, ISIN, In_Data, BlkB, CostB, Inc, Dec, 
                                                        Out, BlkE, CostE, P, T1, T2, T3, TN)
                                                        VALUES
                                                        ({:d}, '{:s}', '{:s}', '{:s}', {:f}, {:f}, {:f}, {:f}, {:f}, {:f}, {:f}, 
                                                        {:f}, '{:s}', {:f}, {:f}, {:f}, 
                                                        {:f});""".format(current_id, security.I, str(security.IS), security.ISIN,
                                                                         security.In, security.BlkB, security.CostB, security.Inc,
                                                                         security.Dec, security.Out, security.BlkE, security.CostE,
                                                                         security.P, security.T1, security.T2, security.T3, security.TN)
                cursor.execute(sqlite_insert_query)

            sqlite_connection.commit()
            cursor.close()
            sqlite_connection.close()
        except sqlite3.Warning as war:
            print("Возникло исключение:", war)
        except sqlite3.Error as err:
            print("Возникла ошибка:", err)

    def save_cash_transactions(self):
        cash_transactions = self.xml_parser.get_cash_transactions()

        try:
            sqlite_connection = sqlite3.connect('database.db')
            cursor = sqlite_connection.cursor()

            for transaction in cash_transactions.transactions:
                if transaction.Inc is None:
                    inc_data = 0.0
                else:
                    inc_data = transaction.Inc

                if transaction.Dec is None:
                    dec_data = 0.0
                else:
                    dec_data = transaction.Dec

                sqlite_insert_query = """INSERT INTO cash_transactions
                                                                    (D, Op, Cur, Inc, Dec, Rub, C)
                                                                    VALUES
                                                                    ('{:s}', '{:s}', '{:s}', {:f}, {:f}, {:f},
                                                                    '{:s}');""".format(transaction.D, transaction.Op, transaction.Cur,
                                                                                     float(inc_data), float(dec_data), float(transaction.Rub),
                                                                                     str(transaction.C))
                cursor.execute(sqlite_insert_query)

            sqlite_connection.commit()
            cursor.close()
            sqlite_connection.close()
        except sqlite3.Warning as war:
            print("Возникло исключение:", war)
        except sqlite3.Error as err:
            print("Возникла ошибка:", err)

    def save_non_trading_securities_operations(self):
        non_trading_securities_operations = self.xml_parser.get_non_trading_securities_operations()

        try:
            sqlite_connection = sqlite3.connect('database.db')
            cursor = sqlite_connection.cursor()

            for operation in non_trading_securities_operations.operations:
                if operation.Inc is None:
                    inc_data = 0.0
                else:
                    inc_data = operation.Inc

                if operation.Dec is None:
                    dec_data = 0.0
                else:
                    dec_data = operation.Dec

                date_D = convert_date(operation.D)

                sqlite_insert_query = """INSERT INTO non_trading_securities_operations
                                                                    (D, I, Op, ISIN, Inc, Dec)
                                                                    VALUES
                                                                    ('{:s}', '{:s}', '{:s}', '{:s}', {:f}, {:f}
                                                                    );""".format(date_D, operation.I, operation.Op,
                                                                                 operation.ISIN, inc_data, dec_data)
                cursor.execute(sqlite_insert_query)

            sqlite_connection.commit()
            cursor.close()
            sqlite_connection.close()
        except sqlite3.Warning as war:
            print("Возникло исключение:", war)
        except sqlite3.Error as err:
            print("Возникла ошибка:", err)

    def save_other_transactions(self):
        other_transactions = self.xml_parser.get_other_transactions()

        try:
            sqlite_connection = sqlite3.connect('database.db')
            cursor = sqlite_connection.cursor()
            sqlite_select_query = """SELECT MAX(other_transactions_summary_id) FROM other_transactions_summary;"""
            cursor.execute(sqlite_select_query)

            last_id = cursor.fetchall()[0][0]

            if last_id is None:
                current_id = 0
            else:
                current_id = last_id + 1

            sqlite_insert_query = """INSERT INTO other_transactions_summary
                                            (other_transactions_summary_id, summary_Name, summary_Fee, summary_CurF)
                                            VALUES
                                            ({:d}, '{:s}', {:f}, '{:s}'
                                            );""".format(current_id, other_transactions.summary.Name, other_transactions.summary.Fee,
                                                         other_transactions.summary.CurF)
            cursor.execute(sqlite_insert_query)

            for transaction in other_transactions.transactions:
                date_D = convert_date(transaction.D)
                time_T = convert_time(transaction.T)
                date_DPayP = convert_date(transaction.DPayP)
                date_DDelP = convert_date(transaction.DDelP)
                date_DPayF = convert_date(transaction.DPayF)
                date_DDelF = convert_date(transaction.DDelF)

                if transaction.Fee is None:
                    fee_data = 0.0
                else:
                    fee_data = transaction.Fee

                if transaction.CurF is None:
                    curf_data = 'None'
                else:
                    curf_data = transaction.CurF

                sqlite_insert_query = """INSERT INTO other_transactions
                                                        (other_transactions_summary_id, D, T, IS_Data, Type, Qty, Pr, Cur, ACY, 
                                                        SPr, SPrA, CurP, Rt, SPay, Fee, CurF, DebtP, DebtD, DPayP, DDelP, 
                                                        DPayF, DDelF, P, TrdN)
                                                        VALUES
                                                        ({:d}, '{:s}', '{:s}', '{:s}', '{:s}', {:f}, {:f}, '{:s}', {:f}, {:f}, {:f}, 
                                                        '{:s}', {:f}, {:f}, {:f}, '{:s}', {:f}, {:f}, '{:s}', '{:s}', '{:s}', '{:s}', '{:s}', 
                                                        {:d});""".format(current_id, date_D, time_T, transaction.IS,
                                                                         transaction.Type, transaction.Qty, transaction.Pr,
                                                                         transaction.Cur, transaction.ACY, transaction.SPr,
                                                                         transaction.SPrA, transaction.CurP, transaction.Rt,
                                                                         transaction.SPay, fee_data, curf_data,
                                                                         transaction.DebtP, transaction.DebtD, date_DPayP,
                                                                         date_DDelP, date_DPayF, date_DDelF, transaction.P,
                                                                         transaction.TrdN)
                cursor.execute(sqlite_insert_query)

            sqlite_connection.commit()
            cursor.close()
            sqlite_connection.close()
        except sqlite3.Warning as war:
            print("Возникло исключение:", war)
        except sqlite3.Error as err:
            print("Возникла ошибка:", err)

    def save_cash_state(self):
        cash_state = self.xml_parser.get_cash_state()

        try:
            sqlite_connection = sqlite3.connect('database.db')
            cursor = sqlite_connection.cursor()
            sqlite_select_query = """SELECT MAX(cash_state_id) FROM cash_state;"""
            cursor.execute(sqlite_select_query)

            last_id = cursor.fetchall()[0][0]

            if last_id is None:
                current_id = 0
            else:
                current_id = last_id + 1

            sqlite_insert_query = """INSERT INTO cash_state
                                            (cash_state_id, summary_Name, summary_TagBSum, summary_TagESum)
                                            VALUES
                                            ({:d}, '{:s}', {:f}, {:f});""".format(current_id, cash_state.summary.Name,
                                                                                float(cash_state.summary.TagBSum), float(cash_state.summary.TagESum))
            cursor.execute(sqlite_insert_query)

            for currency in cash_state.currencies:

                sqlite_insert_query = """INSERT INTO currencies
                                                        (cash_state_id, name, t1, t2, t3, tn, tag_b_SumC, tag_b_Free, 
                                                        tag_b_Blk, tag_b_Mrg, tag_b_Debt, tag_b_Tax, tag_b_Sum, tag_e_SumC,
                                                        tag_e_Free, tag_e_Blk, tag_e_Mrg, tag_e_Debt, tag_e_Tax, tag_e_Sum)
                                                        VALUES
                                                        ({:d}, '{:s}', {:f}, {:f}, {:f}, {:f}, {:f}, {:f}, {:f}, {:f}, {:f}, 
                                                        {:f}, {:f}, {:f}, {:f}, {:f}, {:f}, {:f}, {:f}, {:f}
                                                        );""".format(current_id, currency.name, currency.t1, currency.t2,
                                                                     currency.t3, currency.tn, currency.tag_b.SumC,
                                                                     currency.tag_b.Free, currency.tag_b.Blk, currency.tag_b.Mrg,
                                                                     currency.tag_b.Debt, currency.tag_b.Tax, currency.tag_b.Sum,
                                                                     currency.tag_e.SumC, currency.tag_e.Free, currency.tag_e.Blk,
                                                                     currency.tag_e.Mrg, currency.tag_e.Debt, currency.tag_e.Tax,
                                                                     currency.tag_e.Sum)
                cursor.execute(sqlite_insert_query)

            sqlite_connection.commit()
            cursor.close()
            sqlite_connection.close()
        except sqlite3.Warning as war:
            print("Возникло исключение:", war)
        except sqlite3.Error as err:
            print("Возникла ошибка:", err)

    def save_cash_flow_types(self):
        cash_flow_types = self.xml_parser.get_cash_flow_types()

        try:
            sqlite_connection = sqlite3.connect('database.db')
            cursor = sqlite_connection.cursor()

            for cash_flow in cash_flow_types.cash_flows:
                sqlite_insert_query = """INSERT INTO cash_flow_types
                                                        (Name, MI, MO, SNE, SE, SV, BNE, BE, BV, VM, GO, C, D, BC, AC, Tax, 
                                                        TaxKeep, Oth)
                                                        VALUES
                                                        ('{:s}', {:f}, {:f}, {:f}, {:f}, {:f}, {:f}, {:f}, {:f}, {:f}, {:f}, 
                                                        {:f}, {:f}, {:f}, {:f}, {:f}, {:f}, {:f}
                                                        );""".format(cash_flow.Name, cash_flow.MI, cash_flow.MO, cash_flow.SNE,
                                                                     cash_flow.SE, cash_flow.SV, cash_flow.BNE, cash_flow.BE,
                                                                     cash_flow.BV, cash_flow.VM, cash_flow.GO, cash_flow.C,
                                                                     cash_flow.D, cash_flow.BC, cash_flow.AC, cash_flow.Tax,
                                                                     cash_flow.TaxKeep, cash_flow.Oth)
                cursor.execute(sqlite_insert_query)

            sqlite_connection.commit()
            cursor.close()
            sqlite_connection.close()
        except sqlite3.Warning as war:
            print("Возникло исключение:", war)
        except sqlite3.Error as err:
            print("Возникла ошибка:", err)

    def save_trading_fees(self):
        trading_fees = self.xml_parser.get_trading_fees()

        try:
            sqlite_connection = sqlite3.connect('database.db')
            cursor = sqlite_connection.cursor()

            for fee in trading_fees.fees:
                sqlite_insert_query = """INSERT INTO trading_fees
                                                        (Op, CurF, Fee, C)
                                                        VALUES
                                                        ('{:s}', '{:s}', {:f}, '{:s}');""".format(fee.Op, fee.CurF, fee.Fee, fee.C)
                cursor.execute(sqlite_insert_query)

            sqlite_connection.commit()
            cursor.close()
            sqlite_connection.close()
        except sqlite3.Warning as war:
            print("Возникло исключение:", war)
        except sqlite3.Error as err:
            print("Возникла ошибка:", err)

    def save_transfer_positions_costs(self):
        transfer_positions_costs = self.xml_parser.get_transfer_positions_costs()

        try:
            sqlite_connection = sqlite3.connect('database.db')
            cursor = sqlite_connection.cursor()

            for cost in transfer_positions_costs.transfer_positions:
                sqlite_insert_query = """INSERT INTO transfer_positions_costs
                                                        (Op, Cur, Fee, C)
                                                        VALUES
                                                        ('{:s}', '{:s}', {:f}, '{:s}');""".format(cost.Op, cost.Cur, cost.Fee, cost.C)
                cursor.execute(sqlite_insert_query)

            sqlite_connection.commit()
            cursor.close()
            sqlite_connection.close()
        except sqlite3.Warning as war:
            print("Возникло исключение:", war)
        except sqlite3.Error as err:
            print("Возникла ошибка:", err)

    def save_repo_differences(self):
        repo_differences = self.xml_parser.get_repo_differences()

        try:
            sqlite_connection = sqlite3.connect('database.db')
            cursor = sqlite_connection.cursor()
            sqlite_select_query = """SELECT MAX(repo_differences_id) FROM repo_differences;"""
            cursor.execute(sqlite_select_query)

            last_id = cursor.fetchall()[0][0]

            if last_id is None:
                current_id = 0
            else:
                current_id = last_id + 1

            sqlite_insert_query = """INSERT INTO repo_differences (repo_differences_id) VALUES ({:d});""".format(current_id)
            cursor.execute(sqlite_insert_query)

            for diff in repo_differences.repo_differences:
                if type(diff) == RepoDifferenceR:
                    date_D = convert_date(diff.D)

                    sqlite_insert_query = """INSERT INTO repo_differences_r
                                                                        (repo_differences_id, G, Type, D, IS_Data, V, Qty, 
                                                                        PrS, SumS, PrF, SumF, SumDif)
                                                                        VALUES
                                                                        ({:d}, '{:s}', '{:s}', '{:s}', '{:s}', '{:s}', {:f}, {:f},
                                                                        {:f}, {:f}, {:f}, {:f}
                                                                        );""".format(current_id, diff.G, diff.Type, date_D,
                                                                                     diff.IS, diff.V, diff.Qty, diff.PrS,
                                                                                     diff.SumS, diff.PrF, diff.SumF, diff.SumDif)
                    cursor.execute(sqlite_insert_query)

                elif type(diff) == RepoDifferenceS:
                    if diff.D is None:
                        date_D = 'None'
                    else:
                        date_D = convert_date(diff.D)

                    if diff.SumS is None:
                        sums_data = 0.0
                    else:
                        sums_data = diff.SumS

                    if diff.SumF is None:
                        sumf_data = 0.0
                    else:
                        sumf_data = diff.SumF

                    sqlite_insert_query = """INSERT INTO repo_differences_s
                                                                        (repo_differences_id, G, D, IS_Data, V, SumS, SumF, 
                                                                        SumDif)
                                                                        VALUES
                                                                        ({:d}, '{:s}', '{:s}', '{:s}', '{:s}', {:f}, {:f}, {:f}
                                                                        );""".format(current_id, diff.G, date_D, diff.IS,
                                                                                     diff.V, sums_data, sumf_data, diff.SumDif)
                    cursor.execute(sqlite_insert_query)

            sqlite_connection.commit()
            cursor.close()
            sqlite_connection.close()
        except sqlite3.Warning as war:
            print("Возникло исключение:", war)
        except sqlite3.Error as err:
            print("Возникла ошибка:", err)


    def save_show_section(self):
        show_section = self.xml_parser.get_show_section()

        try:
            sqlite_connection = sqlite3.connect('database.db')
            cursor = sqlite_connection.cursor()

            sqlite_insert_query = """INSERT INTO show_section
                                                            (sh4, sh5, sh6, sh7, sh8, sh9, sh10, sh11, sh12, sh21, sh22, 
                                                            sh23, sh_ref, sh_repo_mma, sh_swap)
                                                            VALUES
                                                            ({:d}, {:d}, {:d}, {:d}, {:d}, {:d}, {:d}, {:d}, {:d}, {:d}, {:d}, 
                                                            {:d}, {:d}, {:d}, {:d}
                                                            );""".format(show_section.sh4, show_section.sh5, show_section.sh6,
                                                                         show_section.sh7, show_section.sh8, show_section.sh9,
                                                                         show_section.sh10, show_section.sh11, show_section.sh12,
                                                                         show_section.sh21, show_section.sh22, show_section.sh23,
                                                                         show_section.sh_ref, show_section.sh_repo_mma, show_section.sh_swap)
            cursor.execute(sqlite_insert_query)

            sqlite_connection.commit()
            cursor.close()
            sqlite_connection.close()
        except sqlite3.Warning as war:
            print("Возникло исключение:", war)
        except sqlite3.Error as err:
            print("Возникла ошибка:", err)

    def save_financial_instruments_evaluation(self):
        fie = self.xml_parser.get_financial_instruments_evaluation()

        try:
            sqlite_connection = sqlite3.connect('database.db')
            cursor = sqlite_connection.cursor()

            sqlite_insert_query = """INSERT INTO financial_instruments_evaluation
                                                        (name, b_go, e_go, bgo, ego)
                                                        VALUES
                                                        ('{:s}', {:f}, {:f}, {:f}, {:f}
                                                        );""".format(fie.name, fie.b_go, fie.e_go, fie.bgo, fie.ego)
            cursor.execute(sqlite_insert_query)

            sqlite_connection.commit()
            cursor.close()
            sqlite_connection.close()
        except sqlite3.Warning as war:
            print("Возникло исключение:", war)
        except sqlite3.Error as err:
            print("Возникла ошибка:", err)

    def save_pending_transactions_evaluation(self):
        pte = self.xml_parser.get_pending_transactions_evaluation()

        try:
            sqlite_connection = sqlite3.connect('database.db')
            cursor = sqlite_connection.cursor()

            sqlite_insert_query = """INSERT INTO pending_transactions_evaluation
                                                        (name, evalb_cb, evale_cb, evalb_pl, evale_pl)
                                                        VALUES
                                                        ('{:s}', {:f}, {:f}, {:f}, {:f} 
                                                        );""".format(pte.name, pte.evalb_cb, pte.evale_cb, pte.evalb_pl,
                                                                     pte.evale_pl)
            cursor.execute(sqlite_insert_query)

            sqlite_connection.commit()
            cursor.close()
            sqlite_connection.close()
        except sqlite3.Warning as war:
            print("Возникло исключение:", war)
        except sqlite3.Error as err:
            print("Возникла ошибка:", err)



# migrations.migrate()
#
# storage = Storage('report.xml')
#
# storage.save_account_info()
#
# sqlite_connection = sqlite3.connect('database.db')
# cursor = sqlite_connection.cursor()
# sqlite_select_table_query = """SELECT * FROM account_info"""
# result = cursor.execute(sqlite_select_table_query).fetchall()
# sqlite_connection.commit()
# cursor.close()
# sqlite_connection.close()
# print(result)

# storage.save_account_state()
# storage.save_account_operations()
# storage.save_securities_state()
# storage.save_cash_transactions()
# storage.save_non_trading_securities_operations()
# storage.save_other_transactions()
# storage.save_cash_state()
# storage.save_cash_flow_types()
# storage.save_trading_fees()
# storage.save_transfer_positions_costs()
# storage.save_repo_differences()
# storage.save_show_section()
# storage.save_financial_instruments_evaluation()
# storage.save_pending_transactions_evaluation()
