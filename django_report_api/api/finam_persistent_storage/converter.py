def convert_date(date):
    result = str(date.year) + '.' + str(date.month) + '.' + str(date.day)
    return result


def convert_time(time):
    result = str(time.hour) + ':' + str(time.minute) + ':' + str(time.second)
    return result
