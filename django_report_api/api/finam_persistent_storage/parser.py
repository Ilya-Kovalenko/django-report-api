import datetime
import xml.etree.ElementTree as ET
from collections import namedtuple
from dataclasses import dataclass
from typing import Tuple

AccountInfo = namedtuple('AccountInfo', ['name', 'id'])

AccountFieldState = namedtuple('State', ['Name', 'B', 'E', 'Delta'])

Commission = namedtuple('Commission', ['D', 'T', 'Op', 'Fee', 'CurF', 'DPayF', 'C'])

Operation = namedtuple('Operation', ['D', 'T', 'IS', 'ISIN', 'Op', 'Qty', 'Pr', 'Cur', 'SPr', 'SPrA', 'CurP', 'Rt',
                                     'SPay', 'DebtP', 'DebtD', 'DPayP', 'DDelP', 'DPayF', 'DDelF', 'M', 'P', 'RqNo',
                                     'TrdN', 'I', 'C'])

OperationsSummary = namedtuple('OperationsSummary', ['Name', 'Fee', 'CurF'])

Security = namedtuple('Security', ['I', 'IS', 'ISIN', 'In', 'BlkB', 'CostB', 'Inc', 'Dec', 'Out', 'BlkE', 'CostE', 'P',
                                   'T1', 'T2', 'T3', 'TN'])

SecuritiesSummary = namedtuple('SecuritiesSummary', ['Name', 'CostB', 'CostE'])

CashTransaction = namedtuple('CashTransaction', ['D', 'Op', 'Cur', 'Inc', 'Dec', 'Rub', 'C'])

NonTradeSecurityOperation = namedtuple('NonTradeSecurityOperation', ['D', 'I', 'Op', 'ISIN', 'Inc', 'Dec'])

OtherTransaction = namedtuple('OtherTransaction', ['D', 'T', 'IS', 'Type', 'Qty', 'Pr', 'Cur', 'ACY', 'SPr', 'SPrA',
                                                   'CurP', 'Rt', 'SPay', 'Fee', 'CurF', 'DebtP', 'DebtD', 'DPayP',
                                                   'DDelP', 'DPayF', 'DDelF', 'P', 'TrdN'])

OtherTransactionsSummary = namedtuple('OtherTransactionsSummary', ['Name', 'Fee', 'CurF'])

CurrencyParams = namedtuple('CurrencyParams', ['SumC', 'Free', 'Blk', 'Mrg', 'Debt', 'Tax', 'Sum'])

CashSummary = namedtuple('CurrenciesSummary', ['Name', 'TagBSum', 'TagESum'])

CashFlow = namedtuple('CashFlow', ['Name', 'MI', 'MO', 'SNE', 'SE', 'SV', 'BNE', 'BE', 'BV', 'VM', 'GO', 'C', 'D', 'BC',
                                   'AC', 'Tax', 'TaxKeep', 'Oth'])

TradingFee = namedtuple('TradingFee', ['Op', 'CurF', 'Fee', 'C'])

TransferPosition = namedtuple('TransferPosition', ['Op', 'Cur', 'Fee', 'C'])

RepoDifferenceR = namedtuple('RepoDifferenceR', ['G', 'Type', 'D', 'IS', 'V', 'Qty', 'PrS', 'SumS', 'PrF', 'SumF',
                                                 'SumDif'])

RepoDifferenceS = namedtuple('RepoDifferenceS', ['G', 'D', 'IS', 'V', 'SumS', 'SumF', 'SumDif'])


@dataclass
class DocRequisites:
    title: str
    text: str
    firm: str
    date_begin: datetime.date
    date_end: datetime.date
    date_create: datetime.date
    amount_days: int


@dataclass
class DocHeader:
    account_info: AccountInfo
    client_info: str
    contract_info: str
    requisites: DocRequisites


@dataclass
class AccountState:
    total: AccountFieldState
    ruble_position: AccountFieldState
    currency_position: AccountFieldState
    pfi_evaluation: AccountFieldState
    securities: AccountFieldState
    pending_liabilities: AccountFieldState
    planned_positions: AccountFieldState


@dataclass
class AccountOperations:
    operations:  Tuple
    commissions: Tuple
    summary: OperationsSummary


@dataclass
class SecuritiesState:
    securities: Tuple
    summary: SecuritiesSummary


@dataclass
class CashTransactions:
    transactions: Tuple


@dataclass
class NonTradingSecuritiesOperations:
    operations: Tuple


@dataclass
class OtherTransactions:
    transactions: Tuple
    summary: OtherTransactionsSummary


@dataclass
class Currency:
    name: str
    t1: float
    t2: float
    t3: float
    tn: float
    tag_b: CurrencyParams
    tag_e: CurrencyParams


@dataclass
class CashState:
    currencies: Tuple
    summary: CashSummary


@dataclass
class CashFlowTypes:
    cash_flows: Tuple


@dataclass
class TradingFees:
    fees: Tuple


@dataclass
class TransferPositionsCosts:
    transfer_positions: Tuple


@dataclass
class RepoDifferences:
    repo_differences: Tuple


@dataclass
class ShowSection:
    sh4: int
    sh5: int
    sh6: int
    sh7: int
    sh8: int
    sh9: int
    sh10: int
    sh11: int
    sh12: int
    sh21: int
    sh22: int
    sh23: int
    sh_ref: int
    sh_repo_mma: int
    sh_swap: int


@dataclass
class FinancialInstrumentsEvaluation:
    name: str
    b_go: float
    e_go: float
    bgo: float
    ego: float


@dataclass
class PendingTransactionsEvaluation:
    name: str
    evalb_cb: float
    evale_cb: float
    evalb_pl: float
    evale_pl: float


class XmlParser:
    def __init__(self, path: str):
        self.path = path
        self.xml_dom = ET.parse(self.path).getroot()

    @staticmethod
    def parse_date(str_date: str) -> datetime.date:
        day, month, year = str_date.split('.')
        return datetime.date(int(year), int(month), int(day))

    @staticmethod
    def parse_time(str_time: str) -> datetime.time:
        hour, minutes, seconds = str_time.split(':')
        return datetime.time(int(hour), int(minutes), int(seconds))

    def get_account_info(self) -> (DocHeader, DocRequisites):
        xml_header = self.xml_dom.find('HEADER_DOC')

        account_info = AccountInfo(xml_header[0].attrib['Name'], xml_header[0].attrib['ID'])
        client_info = xml_header[1].attrib['Name']
        contract_info = xml_header[2].attrib['Name']

        xml_requisites = self.xml_dom.find('DOC_REQUISITES')

        title = xml_requisites.attrib['Title']
        text = xml_requisites.attrib['Text']
        firm = xml_requisites.attrib['Firm']
        date_begin = self.parse_date(xml_requisites.attrib['DateBegin'])
        date_end = self.parse_date(xml_requisites.attrib['DateEnd'])
        date_create = self.parse_date(xml_requisites.attrib['DateCreate'])
        amount_days = int(xml_requisites.attrib['AmountDays'].split()[0][1:])

        requisites = DocRequisites(title, text, firm, date_begin, date_end, date_create, amount_days)
        header = DocHeader(account_info, client_info, contract_info, requisites)

        return header

    def get_account_state(self) -> [AccountState]:
        xml_sections = self.xml_dom.find('SECTIONS')
        xml = xml_sections.find('DB1')

        total = AccountFieldState(xml[0].attrib['Name'], xml[0].attrib['B'], xml[0].attrib['E'],
                                                xml[0].attrib['Delta'])
        ruble_position = AccountFieldState(xml[1].attrib['Name'], xml[1].attrib['B'], xml[1].attrib['E'],
                                                         xml[1].attrib['Delta'])
        currency_position = AccountFieldState(xml[2].attrib['Name'], xml[2].attrib['B'],
                                                            xml[2].attrib['E'], xml[2].attrib['Delta'])
        pfi_evaluation = AccountFieldState(xml[3].attrib['Name'], xml[3].attrib['B'], xml[3].attrib['E'],
                                                         xml[3].attrib['Delta'])
        securities = AccountFieldState(xml[4].attrib['Name'], xml[4].attrib['B'], xml[4].attrib['E'],
                                                     xml[4].attrib['Delta'])
        pending_liabilities = AccountFieldState(xml[5].attrib['Name'], xml[5].attrib['B'],
                                                              xml[5].attrib['E'], xml[5].attrib['Delta'])
        planned_positions = AccountFieldState(xml[6].attrib['Name'], xml[6].attrib['B'],
                                                            xml[6].attrib['E'], xml[6].attrib['Delta'])

        account_state = AccountState(total, ruble_position, currency_position, pfi_evaluation, securities,
                                     pending_liabilities, planned_positions)

        return account_state

    def get_operations(self) -> AccountOperations:
        xml_sections = self.xml_dom.find('SECTIONS')
        xml = xml_sections.find('DB9')
        operations = []
        commissions = []
        summary = None

        for el in xml:

            if el.tag == 'R' and el.attrib['Op'] == 'Комиссия брокерская':
                commissions.append(Commission(self.parse_date(el.attrib['D']), self.parse_time(el.attrib['T']),
                                              el.attrib['Op'], float(el.attrib['Fee']), el.attrib['CurF'],
                                              self.parse_date(el.attrib['DPayF']), el.attrib['C']))
            elif el.tag == 'T':
                summary = OperationsSummary(el.attrib['Name'], float(el.attrib['Fee']), el.attrib['CurF'])

            else:
                operations.append(Operation(self.parse_date(el.attrib['D']), self.parse_time(el.attrib['T']), el.attrib['IS'],
                                            el.attrib['ISIN'], el.attrib['Op'], float(el.attrib['Qty']),
                                            float(el.attrib['Pr']), el.attrib['Cur'], float(el.attrib['SPr']),
                                            float(el.attrib['SPrA']), el.attrib['CurP'], float(el.attrib['Rt']),
                                            float(el.attrib['SPay']), float(el.attrib['DebtP']),
                                            float(el.attrib['DebtD']), self.parse_date(el.attrib['DPayP']),
                                            self.parse_date(el.attrib['DDelP']), self.parse_date(el.attrib['DPayF']),
                                            self.parse_date(el.attrib['DDelF']), el.attrib['M'], el.attrib['P'],
                                            int(el.attrib['RqNo']), int(el.attrib['TrdN']), el.attrib['I'],
                                            el.attrib['C']))

        account_operations = AccountOperations(tuple(operations), tuple(commissions), summary=summary)

        return account_operations

    def get_securities_state(self) -> SecuritiesState:
        xml_sections = self.xml_dom.find('SECTIONS')
        xml = xml_sections.find('DB5')

        securities = []
        summary = None
        for el in xml:
            if el.tag == 'R':

                if 'IS' not in el.attrib.keys():
                    is_element = None
                else:
                    is_element = el.attrib['IS']

                securities.append(Security(el.attrib['I'], is_element, el.attrib['ISIN'], float(el.attrib['In']),
                                           float(el.attrib['BlkB']), float(el.attrib['CostB']), float(el.attrib['Inc']),
                                           float(el.attrib['Dec']), float(el.attrib['Out']), float(el.attrib['BlkE']),
                                           float(el.attrib['CostE']), el.attrib['P'], float(el.attrib['T1']),
                                           float(el.attrib['T2']), float(el.attrib['T3']), float(el.attrib['TN'])))
            else:
                summary = SecuritiesSummary(el.attrib['Name'], float(el.attrib['CostB']), float(el.attrib['CostE']))

        securities_state = SecuritiesState(tuple(securities), summary)

        return securities_state

    def get_cash_transactions(self) -> [CashTransactions]:
        xml_sections = self.xml_dom.find('SECTIONS')
        xml = xml_sections.find('DB7')

        transactions = []

        for el in xml:
            if 'Grouping' not in el.attrib.keys():
                if 'Inc' not in el.attrib.keys():
                    inc_element = None
                else:
                    inc_element = float(el.attrib['Inc'])

                if 'Dec' not in el.attrib.keys():
                    dec_element = None
                else:
                    dec_element = float(el.attrib['Dec'])

                if 'C' not in el.attrib.keys():
                    c_element = None
                else:
                    c_element = el.attrib['C']

                transactions.append(CashTransaction(el.attrib['D'], el.attrib['Op'], el.attrib['Cur'], inc_element,
                                                    dec_element, el.attrib['Rub'], c_element))

        cash_transaction = CashTransactions(tuple(transactions))

        return cash_transaction

    def get_non_trading_securities_operations(self) -> [NonTradingSecuritiesOperations]:
        xml_sections = self.xml_dom.find('SECTIONS')
        xml = xml_sections.find('DB8')

        operations = []

        for el in xml:
            if 'Inc' not in el.attrib.keys():
                inc_element = None
            else:
                inc_element = float(el.attrib['Inc'])

            if 'Dec' not in el.attrib.keys():
                dec_element = None
            else:
                dec_element = float(el.attrib['Dec'])

            operations.append(NonTradeSecurityOperation(self.parse_date(el.attrib['D']), el.attrib['I'], el.attrib['Op'],
                                                        el.attrib['ISIN'], inc_element, dec_element))

        non_trading_operations = NonTradingSecuritiesOperations(tuple(operations))

        return non_trading_operations

    def get_other_transactions(self) -> [OtherTransactions]:
        xml_sections = self.xml_dom.find('SECTIONS')
        xml = xml_sections.find('DB12')

        transactions = []
        summary = None

        for el in xml:
            if el.tag == 'R':
                if 'Fee' not in el.attrib.keys():
                    fee_element = None
                    curf_element = None
                else:
                    fee_element = float(el.attrib['Fee'])
                    curf_element = el.attrib['CurF']

                transactions.append(OtherTransaction(self.parse_date(el.attrib['D']), self.parse_time(el.attrib['T']),
                                                     el.attrib['IS'], el.attrib['Type'], float(el.attrib['Qty']),
                                                     float(el.attrib['Pr']), el.attrib['Cur'], float(el.attrib['ACY']),
                                                     float(el.attrib['SPr']), float(el.attrib['SPrA']), el.attrib['CurP'],
                                                     float(el.attrib['Rt']), float(el.attrib['SPay']), fee_element,
                                                     curf_element, float(el.attrib['DebtP']), float(el.attrib['DebtD']),
                                                     self.parse_date(el.attrib['DPayP']), self.parse_date(el.attrib['DDelP']),
                                                     self.parse_date(el.attrib['DPayF']), self.parse_date(el.attrib['DDelF']),
                                                     el.attrib['P'], int(el.attrib['TrdN'])))
            else:
                summary = OtherTransactionsSummary(el.attrib['Name'], float(el.attrib['Fee']), el.attrib['CurF'])

        other_transactions = OtherTransactions(tuple(transactions), summary)

        return other_transactions

    def get_cash_state(self) -> [CashState]:
        xml_sections = self.xml_dom.find('SECTIONS')
        xml = xml_sections.find('DB20')

        currencies = []
        currency = None
        summary = None

        for el in xml:
            if el.tag == 'R':
                el_b = el.find('B')
                tag_b = CurrencyParams(float(el_b.attrib['SumC']), float(el_b.attrib['Free']),
                                       float(el_b.attrib['Blk']), float(el_b.attrib['Mrg']),
                                       float(el_b.attrib['Debt']), float(el_b.attrib['Tax']),
                                       float(el_b.attrib['Sum']))

                el_e = el.find('E')
                tag_e = CurrencyParams(float(el_e.attrib['SumC']), float(el_e.attrib['Free']),
                                       float(el_e.attrib['Blk']), float(el_e.attrib['Mrg']),
                                       float(el_e.attrib['Debt']), float(el_e.attrib['Tax']),
                                       float(el_e.attrib['Sum']))

                currency = Currency(el.attrib['Name'], float(el.attrib['T1']), float(el.attrib['T2']),
                                    float(el.attrib['T3']), float(el.attrib['TN']), tag_b, tag_e)

                currencies.append(currency)

            else:
                b_sum = el.find('B').attrib['Sum']
                e_sum = el.find('E').attrib['Sum']
                summary = CashSummary(el.attrib['Name'], b_sum, e_sum)

        cash_state = CashState(tuple(currencies), summary)

        return cash_state

    def get_cash_flow_types(self) -> [CashFlowTypes]:
        xml_sections = self.xml_dom.find('SECTIONS')
        xml = xml_sections.find('DB21')

        cash_flows = []

        for el in xml:
            cash_flow = CashFlow(el.attrib['Name'], float(el.attrib['MI']), float(el.attrib['MO']),
                                 float(el.attrib['SNE']), float(el.attrib['SE']), float(el.attrib['SV']),
                                 float(el.attrib['BNE']), float(el.attrib['BE']), float(el.attrib['BV']),
                                 float(el.attrib['VM']), float(el.attrib['GO']), float(el.attrib['C']),
                                 float(el.attrib['D']), float(el.attrib['BC']), float(el.attrib['AC']),
                                 float(el.attrib['Tax']), float(el.attrib['TaxKeep']), float(el.attrib['Oth']))
            cash_flows.append(cash_flow)

        cash_flow_types = CashFlowTypes(tuple(cash_flows))

        return cash_flow_types

    def get_trading_fees(self) -> [TradingFees]:
        xml_sections = self.xml_dom.find('SECTIONS')
        xml = xml_sections.find('DB22')

        fees = []

        for el in xml:
            fee = TradingFee(el.attrib['Op'], el.attrib['CurF'], float(el.attrib['Fee']), el.attrib['C'])
            fees.append(fee)

        trading_fees = TradingFees(tuple(fees))

        return trading_fees

    def get_transfer_positions_costs(self) -> [TransferPositionsCosts]:
        xml_sections = self.xml_dom.find('SECTIONS')
        xml = xml_sections.find('DB23')

        transfers = []

        for el in xml:
            transfer = TransferPosition(el.attrib['Op'], el.attrib['Cur'], float(el.attrib['Fee']), el.attrib['C'])
            transfers.append(transfer)

        transfer_positions = TransferPositionsCosts(tuple(transfers))

        return transfer_positions

    def get_repo_differences(self) -> [RepoDifferences]:
        xml_sections = self.xml_dom.find('SECTIONS')
        xml = xml_sections.find('DB_SWAP')

        differences = []

        for el in xml:
            if el.attrib['G'] == 'R':
                differences.append(RepoDifferenceR(el.attrib['G'], el.attrib['Type'],  self.parse_date(el.attrib['D']),
                                                   el.attrib['IS'], el.attrib['V'], float(el.attrib['Qty']),
                                                   float(el.attrib['PrS']), float(el.attrib['SumS']),
                                                   float(el.attrib['PrF']), float(el.attrib['SumF']),
                                                   float(el.attrib['SumDif'])))
            elif el.attrib['G'] == 'S':
                if 'D' not in el.attrib.keys():
                    d_element = None
                else:
                    d_element = self.parse_date(el.attrib['D'])

                if 'SumS' not in el.attrib.keys():
                    sums_element = None
                else:
                    sums_element = float(el.attrib['SumS'])

                if 'SumF' not in el.attrib.keys():
                    sumf_element = None
                else:
                    sumf_element = float(el.attrib['SumF'])

                differences.append(RepoDifferenceS(el.attrib['G'], d_element, el.attrib['IS'], el.attrib['V'],
                                                   sums_element, sumf_element, float(el.attrib['SumDif'])))

        differences = RepoDifferences(tuple(differences))

        return differences

    def get_show_section(self) -> [ShowSection]:
        xml = self.xml_dom.find('SHOW_SECTION')

        show_section = ShowSection(int(xml.attrib['SH4']),int(xml.attrib['SH5']), int(xml.attrib['SH6']),
                                   int(xml.attrib['SH7']), int(xml.attrib['SH8']), int(xml.attrib['SH9']),
                                   int(xml.attrib['SH10']), int(xml.attrib['SH11']), int(xml.attrib['SH12']),
                                   int(xml.attrib['SH21']), int(xml.attrib['SH22']), int(xml.attrib['SH23']),
                                   int(xml.attrib['SH_REF']), int(xml.attrib['SH_REPO_MMA']), int(xml.attrib['SH_SWAP']))

        return show_section

    def get_financial_instruments_evaluation(self) -> [FinancialInstrumentsEvaluation]:
        xml_sections = self.xml_dom.find('SECTIONS')
        xml = xml_sections.find('DB4').find('T')

        financial_instruments_eval = FinancialInstrumentsEvaluation(xml.attrib['Name'], float(xml.attrib['B_GO']),
                                                                    float(xml.attrib['E_GO']), float(xml.attrib['BGO']),
                                                                    float(xml.attrib['EGO']))

        return financial_instruments_eval

    def get_pending_transactions_evaluation(self) -> [PendingTransactionsEvaluation]:
        xml_sections = self.xml_dom.find('SECTIONS')
        xml = xml_sections.find('DB6').find('T')

        pending_transactions_eval = PendingTransactionsEvaluation(xml.attrib['Name'], float(xml.attrib['EvalB_CB']),
                                                                  float(xml.attrib['EvalE_CB']),
                                                                  float(xml.attrib['EvalB_Pl']),
                                                                  float(xml.attrib['EvalE_Pl']))

        return pending_transactions_eval
