from django.db import models


class ReportFile(models.Model):
    name = models.CharField(max_length=255)
    file = models.FileField(upload_to='reports')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Отчёт'
        verbose_name_plural = 'Отчёты'
