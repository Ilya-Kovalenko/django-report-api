from rest_framework import viewsets, permissions, authentication
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import ReportFile
from .serializers import ReportFileSerializer

import pathlib  # с помощью этой библиотеки узнаем путь до файла


class ReportFileViewSet(viewsets.ModelViewSet):
    queryset = ReportFile.objects.all()
    serializer_class = ReportFileSerializer


# Create your views here.
# class UploadReport(APIView):
#     """
#     View to start vote.
#
#     * Requires token authentication.
#     * Requires Admin rights.
#     """
#     permission_classes = [permissions.IsAuthenticated, permissions.IsAdminUser]
#
#     def get(self, request):
#         if VotesOption.objects.filter(id=1)[0].in_process:
#             return Response({"status": "Vote already started"}, status=200)
#         else:
#             VotesOption.objects.filter(id=1).update(in_process=True, is_finished=False)
#             return Response({"status": "Ok"}, status=200)