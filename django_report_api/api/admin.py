from django.contrib import admin
from .models import ReportFile

admin.site.register(ReportFile)
